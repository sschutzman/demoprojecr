import { LightningElement, track } from "lwc";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
 
export default class orderForm extends LightningElement {

    orderForm = false;
    cakeModal = false;

    @track activeSections = [];
 
    fields_per_section = [
        {
            label: "Contact Details",
            fields: [
                "Name",
                "Phone",
                "MailingAddress"


            ]
        }
    ];

    handleNext(){
        this.orderForm = !this.orderForm;
    }

    handleCancel(event) {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
            this.dispatchEvent(event);
        }
    }
 
    handleSuccess() {
        const event = new ShowToastEvent({
            variant: 'success',
            title: 'Success!',
            message: 'Record has been created',
        });
        this.dispatchEvent(event);
    }
}