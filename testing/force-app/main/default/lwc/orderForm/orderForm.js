import { LightningElement, track, api} from "lwc";
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
// import { NavigationMixin } from 'lightning/navigation';
 
export default class orderForm extends LightningElement {
    @api showCakeOrder;
    @api cakeModal;
    @api cake;
    @track activeSections = ['Order'];

    @ track cakeModal = false;
 
    fields_per_section = [
        {
            label: "Order Details",
            fields: [
                "Contact__c",
                "Cake_Occasion__c",
                "Details",
                "Flavors__c",
                "Filling__c",
                "Frosting__c",
                "Amount__c",
                "Design__c"
            ]
        }
    ];

    get showOrderForm(){
        if(this.showCakeOrder === this.handleNext)
        return false;
        return true;
    }

 
    hideModal() {    
        
        this.cakeModal = false;
        // this[NavigationMixin.Navigate]({
        //     type: 'comm__namedPage',
        //     attributes: {
        //         pageName: 'home'
        //     }
        // });
    }
    
    handleCancel(event) {
        const inputFields = this.template.querySelectorAll(
            'lightning-input-field'
        );
        if (inputFields) {
            inputFields.forEach(field => {
                field.reset();
            });
            this.dispatchEvent(event);
        }
    }
 
    handleSubmit() {
        this.cakeModal = true;
        const event = new ShowToastEvent({
            variant: 'success',
            title: 'Success!',
            message: 'Record has been created',
        });
        this.dispatchEvent(event);
    }
    
}