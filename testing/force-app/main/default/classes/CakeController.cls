global without sharing class CakeController {
    
    public cakeController() {}

    public final Cake__c cake {get;private set;}
    public final Contact contact {get;private set;}
    

    @AuraEnabled(cacheable=true)
    public static Cake__c getCakeContact (Id contactId) {
        // String contactId = [SELECT Id, Name FROM Cake__c WHERE Id = :contactId];
        Cake__c cake = new Cake__c();
        cake = [SELECT Id,Contact__r.FirstName,Contact__r.LastName,Contact__r.Email, Contact__r.MailingAddress
                FROM Cake__c
                WHERE Contact__c = :contactId];
        return cake;

    }

    // @AuraEnabled(cacheable=true)
    // public static Map<String,String> getPicklistValues(String objectName, String fieldName) {
    //     Map<String,String> result = new Map<String,String>();
    //     try {
    //         result = ApexHelper.getPicklistValues(objectName,fieldName);
    //     } catch(Exception e) {
    //         result.put('error',e.getMessage() + e.getStackTraceString());
    //     }
    //     return result;
    // }

        
    }